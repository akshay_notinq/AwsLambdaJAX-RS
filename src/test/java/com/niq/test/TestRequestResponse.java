package com.niq.test;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.niq.lambda.runtime.ApiGateWayRequest;
import com.niq.lambda.runtime.ApiGateWayResponse;

@Data
public class TestRequestResponse {
	@JsonProperty("request")
	protected ApiGateWayRequest request;
	
	@JsonProperty("response")
	protected ApiGateWayResponse response;
	
}
