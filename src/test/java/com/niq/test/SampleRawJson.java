package com.niq.test;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonAnySetter;

public class SampleRawJson extends HashMap<String, Object>implements Serializable{
	@JsonAnySetter
	public void ignored(String name, Object value)  {
		put(name, value);
	}
}
