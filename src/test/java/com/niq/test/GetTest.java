package com.niq.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.codec.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.niq.lambda.handler.TestHandler;
import com.niq.lambda.runtime.ApiGateWayResponse;

public class GetTest {
	static final Logger LOGGER = Logger.getLogger(GetTest.class);
	
	public void testSimpleGet() throws JsonParseException, JsonMappingException, IOException {
		InputStream resourceStream = this.getClass().getResourceAsStream("/tests");
		if(resourceStream != null) {
			List<String> resources = IOUtils.readLines(resourceStream, Charsets.UTF_8.toString());
			Iterator<String> resourcesItr = resources.iterator();
			
			while(resourcesItr.hasNext()) {
				String testResourceName = resourcesItr.next();
				LOGGER.debug("Testing {" + testResourceName + "} :: ");
				
				InputStream is = this.getClass().getResourceAsStream("/tests/" + testResourceName);
				ObjectMapper mapper = new ObjectMapper();
				TestRequestResponse testReqRes = mapper.readValue(is, TestRequestResponse.class);
				TestHandler hh = new TestHandler();
				LOGGER.debug(testReqRes.getRequest().getPath());
				ApiGateWayResponse resp = hh.handleRequest(testReqRes.request, null);
				
				Assert.assertEquals(testReqRes.getResponse(), resp);
				LOGGER.debug("SUCCESS");
			}
		}else {
			LOGGER.debug("No Tests in /tests folder");
		}
	}
	
	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		GetTest ob = new GetTest();
		ob.testSimpleGet();
	}
	
}
