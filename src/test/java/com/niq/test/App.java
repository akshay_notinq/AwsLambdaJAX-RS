package com.niq.test;
import com.niq.lambda.handler.TestHandler;
import com.niq.lambda.runtime.ApiGateWayRequest;
import com.niq.lambda.runtime.ApiGateWayResponse;

public class App {
	public static ApiGateWayResponse testApi(ApiGateWayRequest request) {
		TestHandler hh = new TestHandler();
		ApiGateWayResponse resp = hh.handleRequest(request, null);
		return resp;
	}
}
