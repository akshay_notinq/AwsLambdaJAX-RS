package com.niq.resource;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.json.JSONException;

import com.niq.test.SampleRawJson;
 
@Path("/nodelambada")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HelloWorldResource {
	static final Logger LOGGER = Logger.getLogger(HelloWorldResource.class);
	
    @GET
	@Path("")
	public ResponseBuilder simpleGet() throws JSONException {
    	Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message",  "success");
		
		LOGGER.debug(obj);
        return Response.ok(obj);
    }
    
    @GET
	@Path("{id}")
	public ResponseBuilder simpleGetWithPathParam(@PathParam("id") String myId) throws JSONException {
    	Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message",  "success");
		obj.put("id",  myId);
		
		LOGGER.debug(obj);
        return Response.ok(obj);
    }
    
    @GET
	@Path("{id}/query")
	public ResponseBuilder simpleGetWithQuery(@Context UriInfo uriInfo) throws JSONException {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message",  "success");
		obj.put("query", uriInfo.getQueryParameters());
		
		LOGGER.debug(obj);
        return Response.ok(obj);
    }
    
    
    @POST
   	@Path("")
   	public ResponseBuilder simplePost() throws JSONException {
	   	Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message",  "success");
		
		LOGGER.debug(obj);
	       return Response.ok(obj);
    }
    
    @POST
	@Path("{id}")
	public ResponseBuilder simplePostWithPathParam(@PathParam("id") String myId) throws JSONException {
    	Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message",  "success");
		obj.put("id",  myId);
		
		LOGGER.debug(obj);
        return Response.ok(obj);
    }
    
    @POST
	@Path("{id}/query")
	public ResponseBuilder simplePostWithQuery(@Context UriInfo uriInfo) throws JSONException {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message",  "success");
		obj.put("query", uriInfo.getQueryParameters());
		
		LOGGER.debug(obj);
        return Response.ok(obj);
    }
    

    /*
     * @BeanParam Not Supported
     */
	@POST
	@Path("{id}/rawbody")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
    public ResponseBuilder simplePostWithRawJSON(@PathParam("id") String myId, SampleRawJson postData) throws JSONException {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message",  "success");
		obj.put("postData", postData);
		
		LOGGER.debug(obj);
        return Response.ok(obj);
    }
	
	@POST
	@Path("{id}/rawbody")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
    public ResponseBuilder simplePostWithRawStringBody(@PathParam("id") String myId, String postData) throws JSONException {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message",  "success");
		obj.put("postData", postData);
		Response m = Response.ok(obj).build();
		LOGGER.debug(obj);
        return Response.ok(obj);
    }
	
	@POST
	@Path("{id}/rawbody")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseBuilder simplePostWithUrlEncodedParam(@PathParam("id") String myId, @FormParam("key1") String key1, @FormParam("key2") String key2) throws JSONException {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message",  "success");
		obj.put("key1", key1);
		obj.put("key2", key2);
		Response m = Response.ok(obj).build();
		LOGGER.debug(obj);
        return Response.ok(obj);
    }
	
	
//	
//	@POST  
//    @Path("upload")  
//    @Consumes(MediaType.MULTIPART_FORM_DATA)  
//	@Produces(MediaType.APPLICATION_JSON)
//    public ResponseBuilder uploadFile(@FormDataParam("file") InputStream uploadedInputStream,
//    		@FormDataParam("file") FormDataContentDisposition fileDetail) {  
//		
//		String fileLocation = "/Users/akki/Downloads/saved-" + fileDetail.getFileName();  
//		        //saving file  
//		try {  
//		    FileOutputStream out = new FileOutputStream(new File(fileLocation));  
//		    int read = 0;  
//		    byte[] bytes = new byte[1024];  
//		    out = new FileOutputStream(new File(fileLocation));  
//		    while ((read = uploadedInputStream.read(bytes)) != -1) {  
//		        out.write(bytes, 0, read);  
//		    }  
//		    out.flush();  
//		    out.close();  
//		} catch (IOException e) {e.printStackTrace();}  
//		String output = "File successfully uploaded to : " + fileLocation;  
//		return Response.status(200).entity(output);  
//    } 
//	
//	@POST
//	@Path("exampleFormData")
//	@Consumes(MediaType.MULTIPART_FORM_DATA)
//	@Produces(MediaType.APPLICATION_JSON)
//	public ResponseBuilder uploadFile(@FormDataParam("akshay") String akshay, @FormDataParam("suno") String suno) {  
//		
//		
//		
//		 return Response.ok("asd");
//    } 
//	
//	@POST
//	@Path("exampleUrlEncodedFormData")
//	@Produces(MediaType.APPLICATION_JSON)
//	public ResponseBuilder exampleUrlEncodedFormData(@FormParam("b") String akshay) {  
//		
//		
//		 return Response.ok("ads");
//    } 
}