package com.niq.lambda.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotSupportedException;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.glassfish.jersey.server.model.Invocable;
import org.glassfish.jersey.server.model.ResourceMethod;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.niq.lambda.runtime.ApiGateWayRequest;
import com.niq.lambda.runtime.parser.FormRequestBodyParser;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

public class ResourceMethodInvoker {
	static final Logger LOGGER = Logger.getLogger(ResourceMethodInvoker.class);

    private ResourceMethodInvoker() {
    }

    private static Object toObject(String value, Class<?> clazz) {
        if (clazz == Integer.class || Integer.TYPE == clazz) {
            return value == null ? null : Integer.parseInt(value);
        }
        if (clazz == Long.class || Long.TYPE == clazz) {
            return value == null ? null : Long.parseLong(value);
        }
        if (clazz == Float.class || Float.TYPE == clazz) {
            return value == null ? null : Float.parseFloat(value);
        }
        if (clazz == Boolean.class || Boolean.TYPE == clazz) {
            return value == null ? null : Boolean.parseBoolean(value);
        }
        if (clazz == Double.class || Double.TYPE == clazz) {
            return value == null ? null : Double.parseDouble(value);
        }
        if (clazz == Byte.class || Byte.TYPE == clazz) {
            return value == null ? null : Byte.parseByte(value);
        }
        if (clazz == Short.class || Short.TYPE == clazz) {
            return value == null ? null : Short.parseShort(value);
        }
        return value;
    }
    

    public static Object invoke(ResourceMethod resourceMethod,
                                ApiGateWayRequest request,
                                Context lambdaContext)
            throws
            Exception {

        MediaType produceMediaType = (resourceMethod.getProducedTypes() != null && resourceMethod.getProducedTypes().size() > 0) ? resourceMethod.getProducedTypes().get(0) : MediaType.APPLICATION_JSON_TYPE;
        MediaType consumesMediaType = (resourceMethod.getConsumedTypes() != null && resourceMethod.getConsumedTypes().size() > 0 ) ? resourceMethod.getConsumedTypes().get(0) : MediaType.APPLICATION_JSON_TYPE;
        
        LOGGER.info("Consuming " + consumesMediaType.toString());
        LOGGER.info("Producing " + produceMediaType.toString());
        
        Invocable invocable = resourceMethod.getInvocable();

        Method method = invocable.getHandlingMethod();
        Class<?> clazz = invocable.getHandler().getHandlerClass();

        Object instance = clazz.newInstance();
        
        List<Object> varargs = new ArrayList<Object>();

        for (Parameter parameter : method.getParameters()) {

            Class<?> parameterClass = parameter.getType();
            
            /**
             * Path parameter
             */
            if (parameter.isAnnotationPresent(PathParam.class)) {
                PathParam annotation = parameter.getAnnotation(PathParam.class);
                varargs.add(toObject(
                        request.getPathParameters().get(annotation.value()), parameterClass
                        )
                );

            }

            /**
             * Query parameter
             */
            else if (parameter.isAnnotationPresent(QueryParam.class)) {
                QueryParam annotation = parameter.getAnnotation(QueryParam.class);
                varargs.add(toObject(
                		request.getUriInfo().getQueryParameters().getFirst(annotation.value()), parameterClass
                        )
                );
            }

            /**
             * Header parameter
             */
            else if (parameter.isAnnotationPresent(HeaderParam.class)) {
                HeaderParam annotation = parameter.getAnnotation(HeaderParam.class);
                varargs.add(toObject(
                        request.getHeaders().get(annotation.value()), parameterClass
                        )
                );
            }
            
            else if (parameter.isAnnotationPresent(FormParam.class)) {
            	FormParam annotation = parameter.getAnnotation(FormParam.class);
            	FormRequestBodyParser requestParser = new FormRequestBodyParser(request);
                varargs.add(toObject(
                		requestParser.getFormData().getField(annotation.value()).getValue(), parameterClass
                		));
            }
            
            
            else if (parameter.isAnnotationPresent(javax.ws.rs.core.Context.class)) {
            	javax.ws.rs.core.Context annotation = parameter.getAnnotation(javax.ws.rs.core.Context.class);
                varargs.add(request.getUriInfo());
            }
            
            
            else if (consumesMediaType.equals(MediaType.TEXT_PLAIN_TYPE) && parameterClass == String.class) {
            	varargs.add(toObject(
                		request.getBody(), parameterClass
                		));
            }else if (consumesMediaType.equals(MediaType.APPLICATION_JSON_TYPE) && Serializable.class.isAssignableFrom(parameterClass)) {
            	if (parameterClass == String.class) {
                    //Pass raw request body
                    varargs.add(request.getBody());
                } else {
                	
                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        Object deserializedParameter = mapper.readValue(request.getBody(), parameterClass);
                        varargs.add(deserializedParameter);
                    } catch (IOException ioException) {
                        LOGGER.error("Could not serialized " + request.getBody() + " to " + parameterClass + ":", ioException);
                        varargs.add(null);
                    }
                }
            }else if (consumesMediaType.equals(MediaType.MULTIPART_FORM_DATA_TYPE)) {
                
                    if (parameter.isAnnotationPresent(FormDataParam.class)) {
                    	FormRequestBodyParser requestParser = new FormRequestBodyParser(request);
                    	FormDataParam annotation = parameter.getAnnotation(FormDataParam.class);
                    	
                    	if (parameterClass == String.class) {
                            //Pass raw request body
                    		varargs.add(toObject(
                            		requestParser.getFormData().getField(annotation.value()).getValue(), parameterClass
                            		));
                        } else if( parameterClass == InputStream.class){
                            try {
                            	InputStream stream = new ByteArrayInputStream(requestParser.getFormData().getField(annotation.value()).getValue().getBytes(StandardCharsets.UTF_8));
                            	varargs.add(stream);
                            } catch (Exception ioException) {
                                LOGGER.error("Could not serialized " + request.getBody() + " to " + parameterClass + ":", ioException);
                                varargs.add(null);
                            }
                        }else if( parameterClass == FormDataContentDisposition.class){
                            try {
                            	FormDataContentDisposition p = requestParser.getFormData().getField(annotation.value()).getFormDataContentDisposition();
                            	
                            	
                                varargs.add(p);
                            } catch (Exception ioException) {
                                LOGGER.error("Could not serialized " + request.getBody() + " to " + parameterClass + ":", ioException);
                                varargs.add(null);
                            }
                        }
                        
                } 
            }
            

            else if(parameter.isAnnotationPresent(BeanParam.class)) {
            	throw new NotSupportedException("@BeanParam is not supported");
            }

            /**
             * Lambda Context can be automatically injected
             */
            
            if (parameter.getType() == Context.class) {
                varargs.add(lambdaContext);
            }
        }
        
        LOGGER.debug(varargs);
       
        
        Object resp = method.invoke(instance, varargs.toArray());
        if(resp instanceof Response) {
        	return resp;
        }else if(resp instanceof ResponseBuilder) {
        	((ResponseBuilder) resp).type(produceMediaType);
        	return ((ResponseBuilder) resp).build();
        }
        return resp;
    }
}
