package com.niq.lambda.handler;

import java.lang.reflect.InvocationTargetException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.glassfish.jersey.server.model.ResourceMethod;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.niq.lambda.runtime.ApiGateWayRequest;
import com.niq.lambda.runtime.ApiGateWayResponse;
import com.niq.lambda.runtime.router.Router;


public abstract class AbstractApiGateWayHandler implements RequestHandler<ApiGateWayRequest, ApiGateWayResponse>{
	static final Logger LOGGER = Logger.getLogger(AbstractApiGateWayHandler.class);
	
	private Router router;
	
	public AbstractApiGateWayHandler setRouter(Router router) {
        this.router = router;
        return this;
    }

    public Router getRouter() {
        if (router != null) {
            return router;
        }
        return Router.getRouter(this.getClass());
    }
	
    public abstract String getPackageResource();
    
	public ApiGateWayResponse handleRequest(ApiGateWayRequest request, Context context) {
		try{
			LOGGER.debug("Incoming Request ::: ");
			LOGGER.debug(request);
			
			String resourcePackage = this.getPackageResource();
			resourcePackage = resourcePackage != null ? resourcePackage : "com.niq.resource";
			request.setResourcePackage(resourcePackage);
			
			LOGGER.debug("Setting Resource Package: "+resourcePackage);
			
			
			ResourceMethod matchedResourceMethod = getRouter().route(request);
			
			LOGGER.debug("Matched Resource: "+matchedResourceMethod);
			Object response = ResourceMethodInvoker.invoke(matchedResourceMethod, request, context);
			
			LOGGER.debug("JAX-RS response : "+response);
			if(response instanceof Response) {
				ApiGateWayResponse awsResponse = new ApiGateWayResponse((Response) response);
				LOGGER.debug("ApiGateWayResponse  :: " + awsResponse );
				return awsResponse;
			}else {
				ApiGateWayResponse awsResponse = new ApiGateWayResponse(200, null, response);
				LOGGER.debug("ApiGateWayResponse  :: " + awsResponse );
				return awsResponse;
			}		
		} catch(WebApplicationException e) {
			e.printStackTrace();
			return new ApiGateWayResponse(e.getResponse());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return new ApiGateWayResponse(500, null, "Something very wrong happend");
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return new ApiGateWayResponse(500, null, "Something very wrong 2 happend");
		} catch (InstantiationException e) {
			e.printStackTrace();
			return new ApiGateWayResponse(500, null, "Something very wrong 3 happend");
		} catch (Exception e) {
			e.printStackTrace();
			return new ApiGateWayResponse(500, null, "Something very wrong 4 happend");
		}
	}
}