package com.niq.lambda.handler;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import org.apache.commons.lang.StringEscapeUtils;

public class TempMe {
	public static void main(String[] args) throws IOException {
		String fileLocation = "/Users/akki/Desktop/Screen Shot 2016-12-07 at 10.44.30 PM.png";
		File f = new File(fileLocation);
		byte[] array = Files.readAllBytes(f.toPath());
		String out = new String(array, StandardCharsets.UTF_8);
		String escaped = StringEscapeUtils.escapeJava(out);
		
	}
}