package com.niq.lambda.runtime;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.Getter;
import lombok.Setter;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@Setter
@Getter

public class ApiGateWayResponse implements Serializable {

    /**
     * Response headers
     */
	@JsonProperty("headers")
    protected Map<String, String> headers;

    /**
     * Status code
     */
	@JsonProperty("statusCode")
    protected int statusCode = 200;

    /**
     * Response entity
     */
	@JsonProperty("body")
    protected Object body;
	
	public ApiGateWayResponse() {
		
	}
	
	public ApiGateWayResponse(int statusCode, Map<String, String> headers, Object body) {
		this.statusCode = statusCode;
		this.headers = headers;
		this.body = body;
		
		
		System.err.println("body = " + body);
	}
	
	public ApiGateWayResponse(Response JAXResponse) {
		
		ObjectMapper mapper = new ObjectMapper();
		this.statusCode = JAXResponse.getStatus();
        
		try {
			if(MediaType.TEXT_PLAIN_TYPE.equals(JAXResponse.getMediaType())) {
				this.body = JAXResponse.getEntity();
			}else {
				this.body = mapper.writeValueAsString(JAXResponse.getEntity());
			}
		}catch(Exception e) {
			
			this.body = e.getMessage();
			this.statusCode = 500;
		}
        this.headers = new LinkedHashMap<String, String>();

        for (Map.Entry<String, List<Object>> entry : JAXResponse.getHeaders().entrySet()) {
        	this.headers.put(entry.getKey(), String.valueOf(entry.getValue().get(0) ) );
        }
	}
	
	@Override
	public String toString() {
		try{
			ObjectMapper mapper = new ObjectMapper();
			String str = mapper.writeValueAsString(this);
			JSONObject ob = new JSONObject(str);
			return ob.toString();
		}catch(Exception e) {
			return "Error in ApiGateWayResponse";
		}
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof ApiGateWayResponse)) {
			return false;
		}
		ApiGateWayResponse other = (ApiGateWayResponse) obj;
		return this.toString().equals(obj.toString());
	}
	
	
}
