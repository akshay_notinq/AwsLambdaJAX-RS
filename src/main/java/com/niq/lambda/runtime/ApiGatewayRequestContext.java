package com.niq.lambda.runtime;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import lombok.Data;

import com.amazonaws.HttpMethod;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class ApiGatewayRequestContext implements Serializable{
	@JsonProperty("accountId")
	protected String accountId;
	
	@JsonProperty("resourceId")
	protected String resourceId;
	
	@JsonProperty("stage")
	protected String stage;
	
	@JsonProperty("requestId")
	protected String requestId;
	
	@JsonProperty("identity")
	protected ApiGatewayIdentity identity;
	
	@JsonProperty("sourceIp")
	protected String sourceIp;
	
	@JsonProperty("accessKey")
	protected String accessKey;
	
	@JsonProperty("userArn")
	protected String userArn;
	
	@JsonProperty("userAgent")
	protected String userAgent;
	
	@JsonProperty("user")
	protected String user;
	
	@JsonProperty("cognitoAuthenticationProvider")
	protected String cognitoAuthenticationProvider;
	
	@JsonProperty("cognitoAuthenticationType")
	protected String cognitoAuthenticationType;
	
	@JsonProperty("cognitoIdentityPoolId")
	protected String cognitoIdentityPoolId;
	
	@JsonProperty("cognitoIdentityId")
	protected String cognitoIdentityId;
	
	@JsonProperty("caller")
	protected String caller;
	
	@JsonProperty("apiKey")
	protected String apiKey;
	
	@JsonProperty("")
	protected String resourcePath;
	
	@JsonProperty("")
	protected HttpMethod httpMethod;
	
	@JsonProperty("")
	protected String apiId;
	
	protected Map<String, Object> unknownData;
	
	@JsonAnySetter
	public void ignored(String name, Object value) {
	    // can ignore the 'value' if you only care for the name (though you still need the second parameter)
	    
	    if(unknownData == null) {
	    	unknownData = new LinkedHashMap<String, Object>();
	    }
	    unknownData.put(name, value);
	}
}
