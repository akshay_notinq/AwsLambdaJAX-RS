package com.niq.lambda.runtime;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import lombok.Getter;
import lombok.Setter;

import org.json.JSONObject;

import com.amazonaws.HttpMethod;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@Getter
@Setter
@JsonIgnoreProperties("uriInfo")
public class ApiGateWayRequest implements Serializable{
	
	protected UriInfo uriInfo;
	
	@JsonProperty("resource")
	protected String resource;
	
	@JsonProperty("path")
	protected String path;
	
	@JsonProperty("httpMethod")
	protected HttpMethod httpMethod;
	
	@JsonProperty("headers")
	protected Map<String, String> headers;
	
	@JsonProperty("queryStringParameters")
	protected Map<String, String> queryStringParameters;
	
	@JsonProperty("pathParameters")
	protected Map<String, String> pathParameters;
	
	@JsonProperty("stageVariables")
	protected Map<String, String> stageVariables;
	
	@JsonProperty("requestContext")
	protected ApiGatewayRequestContext requestContext;
	
	@JsonProperty("body")
	protected String body;
	
	@JsonProperty("isBase64Encoded")
	protected boolean isBase64Encoded;
    
	protected String resourcePackage;
	
	protected Map<String, Object> unknownData;
	
	@JsonAnySetter
	public void ignored(String name, Object value) {
	    // can ignore the 'value' if you only care for the name (though you still need the second parameter)
	    
	    if(unknownData == null) {
	    	unknownData = new LinkedHashMap<String, Object>();
	    }
	    unknownData.put(name, value);
	}
	
	public MediaType getContentType() {
		if(this.headers != null && this.headers.containsKey("Content-Type")) {
			return MediaType.valueOf(this.headers.get("Content-Type"));
		}
		return MediaType.APPLICATION_JSON_TYPE;
	}
	
	public MediaType getAcceptType() {
		if(this.headers != null && this.headers.containsKey("Accept")) {
			return MediaType.valueOf(this.headers.get("Accept"));
		}
		return MediaType.APPLICATION_JSON_TYPE;
	}
	
	public String toString(){
		try{
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Include.NON_NULL);
			String str = mapper.writeValueAsString(this);
			JSONObject ob = new JSONObject(str);
			ob.put("unknownData", unknownData);
			
			return ob.toString();
		}catch(Exception e) {
			e.printStackTrace();
			return "Error in ApiGateWayRequest";
		}
	}
	
	public UriInfo getUriInfo() {
		if(this.uriInfo == null) {
			this.uriInfo = new ApiGateWayUriInfo(this);
		}
		return this.uriInfo;
	}
}
