package com.niq.lambda.runtime.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.entity.ContentType;

import com.niq.lambda.runtime.ApiGateWayRequest;
import com.sun.jersey.core.header.ContentDisposition;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;

public class FormRequestBodyParser {
	private String _requestLine;
    FormDataMultiPart parts;
    private ApiGateWayRequest request;
    
    public FormRequestBodyParser(ApiGateWayRequest request) throws Exception {
    	this.request = request;
    	this.parts = new FormDataMultiPart();
    	this.parseRequest();
    }
    
    private void parseRequest() throws Exception {
    	if(this.request == null) {
    		throw new Exception("Invalid Request");
    	}
    	if(request.getBody() == null) {
    		
    		return;
    	}
    	
    	this.parseFormDataRequest();
    }
    
    public FormDataMultiPart getFormData() {
    	return this.parts;
    }
    
    private void parseMultiPartRequest() throws IOException, Exception {
    	
    	
    	ContentType t = ContentType.parse(request.getHeaders().get("content-type"));
//    	
    	String boundry = t.getParameter("boundary");
    	BufferedReader reader = new BufferedReader(new StringReader(request.getBody()));
        setRequestLine(reader.readLine()); // Request-Line ; Section 5.1 // Protocol
        
        while (true) {
        	String nameData = reader.readLine();
        	if(nameData == null) {
        		break;
        	}
        	
        	
        	
        	
        	
        	
        	int idx = nameData.indexOf(":");
            if (idx == -1) {
                throw new Exception("Invalid Header Parameter: " + nameData);
            }
            
            String value = reader.readLine();
            String nextLine = reader.readLine();
            while(nextLine != null && !nextLine.startsWith(_requestLine)) {
            	value = value + "\n" + nextLine;
            	nextLine = reader.readLine();
            }
            value = StringEscapeUtils.unescapeJava(value);
//            
            
            value = new String(value);
//            for(int i = 0;i < value.length() ; i++) {
//            	
//            	
//            }
        	ContentDisposition cd = new ContentDisposition(nameData.substring(idx+1, nameData.length() ));
        	FormDataContentDisposition fcd = new FormDataContentDisposition(nameData.substring(idx+1, nameData.length() ));
        	FormDataBodyPart f = new FormDataBodyPart(fcd, value);
        	
        	parts.bodyPart(f);
        	parts.contentDisposition(cd);
        	if(nextLine != null && getRequestLine().equals(nextLine)) {
        		// Next Param
        		continue;
        	}else {
        		break;
        	}
        }
    }
    
    private void parseFormDataRequest() throws IOException, Exception {
    	if(request.getBody().startsWith("------WebKitFormBoundary")) {
    		this.parseMultiPartRequest();
    	}else {
    		this.parseDirectFormDataRequest();
    	}
    }
    
    private void parseDirectFormDataRequest() throws UnsupportedEncodingException {
    	String[] pairs = request.getBody().split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            FormDataBodyPart f = new FormDataBodyPart(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
            parts.bodyPart(f);
        }
    }
    
    public String getRequestLine() {
        return _requestLine;
    }

    private void setRequestLine(String requestLine) throws Exception {
    	
        if (requestLine == null || requestLine.length() == 0) {
            throw new Exception("Invalid Request-Line: " + requestLine);
        }
        _requestLine = requestLine;
    }
    
}
