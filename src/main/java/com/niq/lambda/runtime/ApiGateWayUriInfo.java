package com.niq.lambda.runtime;

import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.sun.jersey.api.uri.UriBuilderImpl;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ApiGateWayUriInfo implements UriInfo{
	protected ApiGateWayRequest request;
	
	public ApiGateWayUriInfo(ApiGateWayRequest request) {
		this.request = request;
	}

	@Override
	public String getPath() {
		// TODO Auto-generated method stub
		return this.getPath(true);
	}

	@Override
	public String getPath(boolean decode) {
		// TODO Auto-generated method stub
		return this.request.getPath();
	}

	@Override
	public List<PathSegment> getPathSegments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PathSegment> getPathSegments(boolean decode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URI getRequestUri() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UriBuilder getRequestUriBuilder() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URI getAbsolutePath() {
		// TODO Auto-generated method stub
		return this.getAbsolutePathBuilder().buildFromMap(this.request.getPathParameters());
	}

	@Override
	public UriBuilder getAbsolutePathBuilder() {
		// TODO Auto-generated method stub
		UriBuilder builder = this.getBaseUriBuilder();
		Map<String, String> queryMap = this.request.getQueryStringParameters();
		
		Iterator<Entry<String, String>> queryDataItr = queryMap.entrySet().iterator();
		while(queryDataItr.hasNext()) {
			Entry<String, String> query = queryDataItr.next();
			builder.queryParam(query.getKey(), query.getValue());
		}
		
		return builder;
	}

	@Override
	public URI getBaseUri() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UriBuilder getBaseUriBuilder() {
		// TODO Auto-generated method stub
		UriBuilderImpl builder = new UriBuilderImpl();
		builder.path(this.request.getPath());
		return builder;
	}

	@Override
	public MultivaluedMap<String, String> getPathParameters() {
		// TODO Auto-generated method stub
		return this.getPathParameters(true);
	}

	@Override
	public MultivaluedMap<String, String> getPathParameters(boolean decode) {
		// TODO Auto-generated method stub
		MultivaluedMap<String, String> mum = new MultivaluedMapImpl();
		Map<String, String> pathParams = this.request.getPathParameters();
		Iterator<Entry<String, String>> pathParamsItr = pathParams.entrySet().iterator();
		while(pathParamsItr.hasNext()) {
			Entry<String, String> pathParam = pathParamsItr.next();
			mum.add(pathParam.getKey(), pathParam.getValue());
		}
		return mum;
	}

	@Override
	public MultivaluedMap<String, String> getQueryParameters() {
		// TODO Auto-generated method stub
		return this.getQueryParameters(true);
	}

	@Override
	public MultivaluedMap<String, String> getQueryParameters(boolean decode) {
		// TODO Auto-generated method stub
		MultivaluedMap<String, String> mum = new MultivaluedMapImpl();
		Map<String, String> queryParams = this.request.getQueryStringParameters();
		Iterator<Entry<String, String>> queryParamsItr = queryParams.entrySet().iterator();
		while(queryParamsItr.hasNext()) {
			Entry<String, String> queryParam = queryParamsItr.next();
			mum.add(queryParam.getKey(), queryParam.getValue());
		}
		return mum;
	}

	@Override
	public List<String> getMatchedURIs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getMatchedURIs(boolean decode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getMatchedResources() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URI resolve(URI uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URI relativize(URI uri) {
		// TODO Auto-generated method stub
		return null;
	}
}
