package com.niq.lambda.runtime;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiGatewayIdentity {
	@JsonProperty("cognitoIdentityPoolId")
	protected String cognitoIdentityPoolId;
	
	@JsonProperty("accountId")
	protected String accountId;
	
	@JsonProperty("cognitoIdentityId")
	protected String cognitoIdentityId;
	
	@JsonProperty("caller")
	protected String caller;
	
	@JsonProperty("apiKey")
	protected String apiKey;
	
	@JsonProperty("sourceIp")
	protected String sourceIp;
	
	@JsonProperty("accessKey")
	protected String accessKey;
	
	@JsonProperty("cognitoAuthenticationType")
	protected String cognitoAuthenticationType;
	
	@JsonProperty("cognitoAuthenticationProvider")
	protected String cognitoAuthenticationProvider;
	
	@JsonProperty("userArn")
	protected String userArn;
	
	@JsonProperty("userAgent")
	protected String userAgent;
	
	@JsonProperty("user")
	protected String user;
	
	protected Map<String, Object> unknownData;
	
	@JsonAnySetter
	public void ignored(String name, Object value) {
	    // can ignore the 'value' if you only care for the name (though you still need the second parameter)
	    
	    if(unknownData == null) {
	    	unknownData = new LinkedHashMap<String, Object>();
	    }
	    unknownData.put(name, value);
	}
}
