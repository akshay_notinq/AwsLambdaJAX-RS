package com.niq.lambda.runtime.router;

import org.glassfish.jersey.server.model.ResourceMethod;

import com.niq.lambda.runtime.ApiGateWayRequest;

public class MethodType implements RouterType {

	@Override
	public boolean isMatching(ApiGateWayRequest request,
			ResourceMethod resourceMethod) {
		 return resourceMethod.getHttpMethod().equals(request.getHttpMethod().name());
	}

}
