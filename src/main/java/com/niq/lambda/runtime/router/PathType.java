package com.niq.lambda.runtime.router;

import java.util.HashMap;

import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;
import org.glassfish.jersey.uri.UriTemplate;

import com.niq.lambda.runtime.ApiGateWayRequest;

public class PathType implements RouterType {
	private static final String SLASH_CHARACTER = "/";

	@Override
	public boolean isMatching(ApiGateWayRequest request,
			ResourceMethod resourceMethod) {
        try {
        	
        	String fullResourcePath = getFullResourcePath(resourceMethod);
        	return new UriTemplate(fullResourcePath).match(request.getPath(), request.getPathParameters() != null ? request.getPathParameters() : new HashMap<String, String>() );
        } catch (NullPointerException e) {
        	
            return false;
        }
	}
	
	public String getFullResourcePath(ResourceMethod resourceMethod) {
		if(resourceMethod != null) {
			StringBuilder sb = new StringBuilder();
			
			getFullResourcePath(resourceMethod.getParent(), sb);
			return sb.toString();
		}else {
			return null;
		}
	}
	
	public void getFullResourcePath(Resource resource, StringBuilder sb) {
		if(resource == null) {
			sb.append("");
			return;
		}
		if(resource.getParent() != null && resource.getParent().getPath() != null && !resource.getParent().getPath().equals(SLASH_CHARACTER)) {
			getFullResourcePath(resource.getParent(), sb);
		}
		
		if(!"".equals(resource.getPath()) && sb.length() > 1) {
			sb.append(SLASH_CHARACTER);
		}
		sb.append(resource.getPath());
	}

}
