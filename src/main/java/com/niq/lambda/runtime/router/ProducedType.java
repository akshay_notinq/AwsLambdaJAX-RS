package com.niq.lambda.runtime.router;

import org.glassfish.jersey.server.model.ResourceMethod;

import com.niq.lambda.runtime.ApiGateWayRequest;

public class ProducedType implements RouterType {

	@Override
	public boolean isMatching(ApiGateWayRequest request,
			ResourceMethod resourceMethod) {
		 return "*/*".equals(request.getAcceptType().toString()) || resourceMethod.getProducedTypes().contains(request.getAcceptType());
	}
	
}
