package com.niq.lambda.runtime.router;

import org.glassfish.jersey.server.model.ResourceMethod;

import com.niq.lambda.runtime.ApiGateWayRequest;

public interface RouterType {
	boolean isMatching(ApiGateWayRequest request, ResourceMethod resourceMethod);
}
