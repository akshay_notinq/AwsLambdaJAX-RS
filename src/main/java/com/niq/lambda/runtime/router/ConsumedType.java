package com.niq.lambda.runtime.router;

import org.glassfish.jersey.server.model.ResourceMethod;

import com.niq.lambda.runtime.ApiGateWayRequest;

public class ConsumedType implements RouterType {

	@Override
	public boolean isMatching(ApiGateWayRequest request,
			ResourceMethod resourceMethod) {
		return resourceMethod.getConsumedTypes().contains(request.getContentType());
	}

}
