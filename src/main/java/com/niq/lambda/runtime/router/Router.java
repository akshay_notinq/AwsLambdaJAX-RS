package com.niq.lambda.runtime.router;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.NotFoundException;

import org.apache.log4j.Logger;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;

import com.niq.lambda.runtime.ApiGateWayRequest;
import com.niq.lambda.runtime.jaxrs.JAXRSParser;

public class Router {
	static final Logger LOOGER = Logger.getLogger(Router.class);

	private static Router singletonInstance = null;

	private Map<String, List<Resource>> resourceMap = new ConcurrentHashMap<>();

	private Map<String, ResourceMethod> routingCache = new ConcurrentHashMap<>();

	private List<RouterType> routerTypes = new LinkedList<>();

	JAXRSParser jaxrsParser;

	Class clazz;

	public Router setJaxrsParser(JAXRSParser jaxrsParser) {
		this.jaxrsParser = jaxrsParser;
		return this;
	}

	/**
	 * Gets the singleton instance
	 * <p>
	 * Why do we use singleton? Because Handler class is directly invoked by AWS
	 * Lambda runtime and we do not have a chance to inject its dependencies.
	 *
	 * @return Router
	 */
	public static Router getRouter(Class clazz) {
		if (singletonInstance != null) {
			LOOGER.debug("Singleton router is being returned.");
			return singletonInstance;
		}

		return singletonInstance = new Router(clazz);
	}

	private Router(Class clazz) {
		LOOGER.debug("Router is being initialized.");
		this.clazz = clazz;
		jaxrsParser = new JAXRSParser();
		addRouterTypes();
	}

	private void addRouterTypes() {
		LOOGER.debug("Adding router types.");
		routerTypes.add(new PathType());
		routerTypes.add(new MethodType());
		routerTypes.add(new ConsumedType());
		routerTypes.add(new ProducedType());
		LOOGER.debug("Router types are added.");
	}

	/**
	 * Scans package for Resources
	 *
	 * @param packageName
	 *            Package name to scan
	 * @return Found resources
	 */
	protected List<Resource> getJAXRSResourcesFromPackage(String packageName) {

		LOOGER.debug("Package is being scanned: " + packageName);

		if (null != resourceMap.get(packageName)) {
			/**
			 * This package is already scanned, so return cached values
			 */
			LOOGER.debug("Returning cached resource map.");
			return resourceMap.get(packageName);
		}

		LOOGER.debug("Cached resource map not found. Scanning package.");
		JAXRSParser jaxrsParser = this.jaxrsParser.withPackageName(packageName,
				this.clazz);
		List<Resource> foundResources = jaxrsParser.scan();
		resourceMap.put(packageName, foundResources);
		LOOGER.debug(foundResources.size() + " resources found.");
		return foundResources;
	}

	private boolean isResourceMapMatches(ApiGateWayRequest request,
			ResourceMethod resourceMethod) {
		String methodName = resourceMethod.getInvocable().getDefinitionMethod().getName();
		List<String> passedRouterTypes = new ArrayList<String>();
		for (RouterType routerType : routerTypes) {
			if (!routerType.isMatching(request, resourceMethod)) {
				LOOGER.debug(methodName + "**FAIL**" + routerType.toString());
				return false;
			}
			passedRouterTypes.add(routerType.toString());
		}
		LOOGER.info(methodName + passedRouterTypes);
		return true;
	}

	private String calculateCacheKeyForRequest(ApiGateWayRequest request) {
		return request.getHttpMethod() + "-" + request.getResource() + "-"
				+ request.getPath() + "-" + request.getContentType().toString() + "-" + request.getAcceptType().toString();
	}

	public ResourceMethod route(ApiGateWayRequest request)
			throws NotFoundException {

		if (request.getResourcePackage() == null) {
			throw new NotFoundException("Request should have package attribute");
		}

		String cacheKey = calculateCacheKeyForRequest(request);
		ResourceMethod foundMethod;

		LOOGER.debug("Matching request with a corresponding resource method.");

		if ((foundMethod = routingCache.get(cacheKey)) != null) {
			LOOGER.info("Request is already cached: "
					+ foundMethod.getClass().toString());
			return foundMethod;
		}

		List<Resource> foundResources = getJAXRSResourcesFromPackage(request
				.getResourcePackage());

		for (Resource resource : foundResources) {
			for (ResourceMethod resourceMethod : resource.getResourceMethods()) {
				//
				if (isResourceMapMatches(request, resourceMethod)) {
					LOOGER.debug("Match complete: "
							+ resourceMethod.getClass().toString());
					routingCache.put(cacheKey, resourceMethod);
					return resourceMethod;
				}
			}
		}

		throw new NotFoundException();
	}
}
